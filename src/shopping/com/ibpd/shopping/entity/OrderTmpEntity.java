package com.ibpd.shopping.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ibpd.entity.baseEntity.IBaseEntity;

@Entity
@Table(name="t_s_ordertmp")
public class OrderTmpEntity  extends IBaseEntity{
	/**
	 *  
	 */
	private static final long serialVersionUID = 1L;

	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="f_tmpOrderNumber",length=18,nullable=true)
	private String tmpOrderNumber="";
	@Column(name="f_orderId",nullable=true)
	private Long orderId=-1L;
	@Column(name="f_orderNumber",length=18,nullable=true)
	private String orderNumber="";
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setTmpOrderNumber(String tmpOrderNumber) {
		this.tmpOrderNumber = tmpOrderNumber;
	}
	public String getTmpOrderNumber() {
		return tmpOrderNumber;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
}
