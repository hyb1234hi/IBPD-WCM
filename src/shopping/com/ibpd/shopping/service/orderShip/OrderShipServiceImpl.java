package com.ibpd.shopping.service.orderShip;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.shopping.entity.OrderShipEntity;
@Service("orderShipService")
public class OrderShipServiceImpl extends BaseServiceImpl<OrderShipEntity> implements IOrderShipService {
	public OrderShipServiceImpl(){
		super();
		this.tableName="OrderShipEntity";
		this.currentClass=OrderShipEntity.class;
		this.initOK();
	}

	public OrderShipEntity getOrderShipByOrderId(Long orderId) {
		List<OrderShipEntity> lst=getList("from "+getTableName()+" where orderid="+orderId,null);
		if(lst==null || lst.size()==0){
			return null;
		}else{
			return lst.get(0);
		}
	}

	public void deleteByOrderId(Long orderId) {
		OrderShipEntity o=getOrderShipByOrderId(orderId);
		if(o!=null){
			this.deleteByPK(o.getId());
		}
		
	}
}
