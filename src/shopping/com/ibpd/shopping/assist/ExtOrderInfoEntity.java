package com.ibpd.shopping.assist;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;

import com.ibpd.shopping.entity.OrderEntity;
import com.ibpd.shopping.entity.OrderShipEntity;
import com.ibpd.shopping.entity.OrderdetailEntity;
import com.ibpd.shopping.entity.OrderpayEntity;

public class ExtOrderInfoEntity extends OrderEntity {

	private List<OrderdetailEntity> detailList=new ArrayList<OrderdetailEntity>();
	private OrderpayEntity orderpay=null;
	private OrderShipEntity orderShip=null;
	public ExtOrderInfoEntity(OrderEntity order){
		try {
			InterfaceUtil.swap(order, this);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public List<OrderdetailEntity> getDetailList() {
		return detailList;
	}
	public void setDetailList(List<OrderdetailEntity> detailList) {
		this.detailList = detailList;
	}
	public OrderpayEntity getOrderpay() {
		return orderpay;
	}
	public void setOrderpay(OrderpayEntity orderpay) {
		this.orderpay = orderpay;
	}
	public OrderShipEntity getOrderShip() {
		return orderShip;
	}
	public void setOrderShip(OrderShipEntity orderShip) {
		this.orderShip = orderShip;
	}
	@Override
	public String toString() {
		JSONArray a=JSONArray.fromObject(this);
		return a.toString();
	}
	
}
