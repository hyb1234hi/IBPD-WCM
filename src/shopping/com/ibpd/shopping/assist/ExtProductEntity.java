package com.ibpd.shopping.assist;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;

import com.ibpd.henuocms.web.controller.manage.BaseController;
import com.ibpd.shopping.entity.ProductEntity;
import com.ibpd.shopping.entity.SpecEntity;

public class ExtProductEntity  extends ProductEntity{

	private List<ExtProductAttrEntity> attributeList=new ArrayList<ExtProductAttrEntity>();
	private List<SpecEntity> specList=new ArrayList<SpecEntity>();
	private String tenantName="";
	private String tenantLogo="";
	private Long userId=-1L;
	private Boolean isFavorite=false;
	public ExtProductEntity(ProductEntity p, List<ExtProductAttrEntity> attrList,List<SpecEntity> specList) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		this.setAttributeList(attrList);
		this.setSpecList(specList);
		(new BaseController()).swap(p, this);
		
	}

	public void setAttributeList(List<ExtProductAttrEntity> attributeList) {
		this.attributeList = attributeList;
	}

	public List<ExtProductAttrEntity> getAttributeList() {
		return attributeList;
	}

	public void setSpecList(List<SpecEntity> specList) {
		this.specList = specList;
	}

	public List<SpecEntity> getSpecList() {
		return specList;
	}


	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getTenantLogo() {
		return tenantLogo;
	}

	public void setTenantLogo(String tenantLogo) {
		this.tenantLogo = tenantLogo;
	}

	@Override
	public String toString() {
		JSONArray j=JSONArray.fromObject(this);
		return j.toString();
	}

	public void setIsFavorite(Boolean isFavorite) {
		this.isFavorite = isFavorite;
	}

	public Boolean getIsFavorite() {
		return isFavorite;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getUserId() {
		return userId;
	}
}
