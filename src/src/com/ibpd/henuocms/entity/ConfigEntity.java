package com.ibpd.henuocms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType; 
import javax.persistence.Id;
import javax.persistence.Table;

import com.ibpd.entity.baseEntity.IBaseEntity;

/**
 * 系统配置
 * @author MG
 * @version 1.0
 */
@Entity
@Table(name="T_Config")
public class ConfigEntity  extends IBaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/*
	 * 站点名称
	 * 站点状态
	 * 站点开启规则（永久开启、定时开启-永不关闭、定时开启-定时关闭、定时关闭）
	 * 定时开启方式（每天、每周、每月、每年）
	 * 定时开启日期(每天：X时X分；每周：1234567；每月：1-31；每年：X月X日）
	 * 定时关闭方式（每天、每周、每月、每年）
	 * 定时关闭日期(每天：X时X分；每周：1234567；每月：1-31；每年：X月X日）
	 * 站点关键字
	 * 站点说明
	 * 自动生成静态页（自动、手动生成）
	 * 首页静态化
	 * 栏目页静态化
	 * 内容页静态化
	 * 表单页静态化
	 * 其他页面静态化
	 * 使用模板id
	 * 上传图片是否加水印
	 * 水印图片
	 * 水印位置
	 * 水印透明度
	 * 不加水印图片的大小
	 * 全站变灰
	 * 上传图片自动压缩（自动、手动）
	 * 上传图片自动转换格式
	 * 上传视频自动转换为FLV格式
	 * 定期自动备份网站文件
	 * 定时执行方式（一次性、每天、每周、每月、每年）
	 * 定时执行日期(一次性：X年X月X日，每周：1234567；每月：1-31；每年：X月X日）
	 * 定期自动备份网站数据库信息
	 * 定时执行方式（一次性、每天、每周、每月、每年）
	 * 定时执行日期(一次性：X年X月X日，每周：1234567；每月：1-31；每年：X月X日）
	 * 定期清理数据库
	 * 定时执行方式（一次性、每天、每周、每月、每年）
	 * 定时执行日期(一次性：X年X月X日，每周：1234567；每月：1-31；每年：X月X日）
	 * 连接数据库类型
	 * 数据库连接URL
	 * 数据库名称
	 * 数据库用户名
	 * 数据库密码
	 * 信息是否自动发布到FTP服务器
	 * FTP地址
	 * FTP端口号
	 * FTP用户名
	 * FTP密码
	 * FTP网站根目录
	 * 是否开启站点访问量统计
*/

	@Id @Column(name="f_id") @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	@Column(name="f_confName",length=255,nullable=true)
	private String confName=" ";
	@Column(name="f_confKey",length=255,nullable=true)
	private String confKey=" ";
	@Column(name="f_confValue",length=255,nullable=true)
	private String confValue=" ";
	@Column(name="f_remark",length=255,nullable=true)
	private String remark=" ";
	@Column(name="f_group",length=255,nullable=true)
	private String group=" ";
	@Column(name="f_order",nullable=true)
	private Integer order=0;
	public void setConfName(String confName) {
		this.confName = confName;
	}
	public String getConfName() {
		return confName;
	}
	public void setConfKey(String confKey) {
		this.confKey = confKey;
	}
	public String getConfKey() {
		return confKey;
	}
	public void setConfValue(String confValue) {
		this.confValue = confValue;
	}
	public String getConfValue() {
		return confValue;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getId() {
		return id;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getRemark() {
		return remark;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	public Integer getOrder() {
		return order;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getGroup() {
		return group;
	}
}
