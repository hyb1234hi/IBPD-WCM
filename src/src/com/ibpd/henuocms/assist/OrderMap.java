package com.ibpd.henuocms.assist;

import java.util.HashMap;
import java.util.Map;

public class OrderMap{
	public OrderMap(){
		oMap=new HashMap<String,String>();
	}
	private Map<String,String> oMap=new HashMap<String,String>();

	public Map<String, String> getoMap() {
		return oMap;
	}

	public void setoMap(Map<String, String> oMap) {
		this.oMap = oMap;
	}
}
