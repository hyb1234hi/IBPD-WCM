package com.ibpd.henuocms.tlds;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTag;
import javax.servlet.jsp.tagext.IterationTag;
import javax.servlet.jsp.tagext.Tag;

import org.hsqldb.lib.StringUtil;

import com.ibpd.dao.impl.ServiceProxyFactory;
import com.ibpd.henuocms.common.ListSortUtil;
import com.ibpd.henuocms.entity.ext.ContentExtEntity;
import com.ibpd.henuocms.entity.ext.NodeExtEntity;
import com.ibpd.henuocms.service.content.ContentServiceImpl;
import com.ibpd.henuocms.service.content.IContentService;
import com.ibpd.henuocms.service.node.INodeService;
import com.ibpd.henuocms.service.node.NodeServiceImpl;
/** 
 * 内容列表tag 废弃
 * @author mg by qq:349070443
 *编辑于 2015-6-20 下午05:42:40
 */
public class ContentListTag extends BaseTag {
	public static String defaultNodeId="OWNER";
	private String nodeId=defaultNodeId;
	private String var="content";
	private String orderField="lastUpdateDate";
	private String orderType="desc";
	private String pageSize="10";
	private String pageIndex="0";
	
	private Integer currentPageSize=10;
	private Integer currentPageIndex=0;
	@Override
	public int doAfterBody() throws JspException {
        Object obj=getNextObj();
        if (obj!=null) {
        	this.pageContext.removeAttribute(var);
            this.pageContext.setAttribute(var, obj);
            return IterationTag.EVAL_BODY_AGAIN;
        }else{
        	this.pageContext.removeAttribute(var);
        	return  Tag.SKIP_BODY;
        }
    }

	@Override
	public int doEndTag() throws JspException {
        try {
            BodyContent bc = getBodyContent();
            if(!bc.getString().trim().equals("")){
         	   String rtn=bc.getString();
         	   rtn=rtn.replace("\r\n", "");
         	   bc.getEnclosingWriter().write (rtn);
            }
         } catch (IOException ie) {
             throw new JspException (ie.getMessage ());
         }
     return Tag.EVAL_PAGE;
	}

	@Override
	public int doStartTag() throws JspException {
		init();
        return BodyTag.EVAL_BODY_BUFFERED;
	}
	protected void  init(){
		if(StringUtil.isEmpty(var)){
			var="content";
		}
		if(StringUtil.isEmpty(orderField)){
			orderField="lastUpdateDate";
		}
		if(StringUtil.isEmpty(orderType)){
			orderType="desc";
		}else{
			if(!orderType.toLowerCase().trim().equals("asc") || !orderType.toLowerCase().trim().equals("desc")){
				orderType="desc";
			}
		}
		if(isNumeric(pageSize)){
			currentPageSize=Integer.parseInt(pageSize);
		}else{
			currentPageSize=10;
		}
		if(isNumeric(pageIndex)){
			currentPageIndex=Integer.parseInt(pageIndex);
		}else{
			currentPageIndex=0;
		}
			Long currentNodeId=-1L;
			this.setNodeId(this.getNodeId()==null?defaultNodeId:this.getNodeId());
			if(this.getNodeId().trim().toUpperCase().equals(this.defaultNodeId)){
				String tmp=pageContext.getRequest().getParameter("nodeId");
				if(!StringUtil.isEmpty(tmp)){
					if(isNumeric(tmp)){
						currentNodeId=Long.parseLong(tmp);
					}
				}
			}else{
				if(isNumeric(this.getNodeId())){
					currentNodeId=Long.parseLong(this.getNodeId());
				}
			}
			IContentService contService=(IContentService) ServiceProxyFactory.getServiceProxy(ContentServiceImpl.class);
			List<ContentExtEntity> contList=contService.getContentList(currentNodeId,currentPageSize,currentPageIndex,orderField,orderType);
			if(contList.size()>0){
				this.objList=contList.iterator();
			}
	}
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public String getNodeId() {
		return nodeId;
	}
	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}
	public String getOrderField() {
		return orderField;
	}

	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getPageSize() {
		return pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(String pageIndex) {
		this.pageIndex = pageIndex;
	}




}
