package com.ibpd.henuocms.service.paper;

import org.springframework.stereotype.Service;

import com.ibpd.dao.impl.BaseServiceImpl;
import com.ibpd.henuocms.entity.PaperEntity;
@Service("paperService")
public class PaperServiceImpl extends BaseServiceImpl<PaperEntity> implements IPaperService {
	public PaperServiceImpl(){
		super();
		this.tableName="PaperEntity";
		this.currentClass=PaperEntity.class;
		this.initOK();
	} 
	
}
