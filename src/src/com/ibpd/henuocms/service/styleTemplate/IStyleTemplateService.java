package com.ibpd.henuocms.service.styleTemplate;

import java.util.List;

import com.ibpd.dao.impl.IBaseService;
import com.ibpd.henuocms.entity.StyleTemplateEntity;

public interface IStyleTemplateService extends IBaseService<StyleTemplateEntity> {
	List<StyleTemplateEntity> getList(Long[] siteIds,Long NodeId,Integer pageSize,Integer pageIndex,String orderType,String filterString);
}
 