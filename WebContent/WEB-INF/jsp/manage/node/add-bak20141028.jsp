<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>栏目管理</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		
			
		<style type="text/css">
		.easyui-tabs table{
			width:500px;
			margin:10 10 10 10;
		}
		.easyui-tabs table tr .tit{
			width:30%;
		} 
		.easyui-tabs table tr .val{
			width:60%;
		} 
		.easyui-tabs table tr .val input{
			margin:2 3 2 3;
			width:96%;
		} 
		.easyui-tabs table tr .val select{
			margin:2 3 2 3;
			width:96%;
		} 
		</style>
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/jquery.form.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/entity/node.js"></script>
	</head>

	<body style="margin:0;padding:0;text-align:center;">
	
	<div class="easyui-layout" fit="true">
   <div region="west" hide="true" split="false" fit="true" id="west">
	<div class="easyui-tabs"  fit="true" border="false" id="fm">
	<div title="基本信息">
    	<input type="hidden" id="entityId" name="id" value="${entity.id }"/>
    	<input type="hidden" name="parentId" id="parentId" value="${parentId }"/>
    	<table cellpadding="0" cellspacing="0" border="0">
				<c:forEach items="${base }" var="field">
				<c:if test="${field.displayByAddMode==true }">
					<tr <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
						<td class="tit" <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>><div class="tittext">${field.displayName }</div></td>
						<td class="val">
							
							<c:if test="${field.tagType=='0' && field.displayByAddMode==true }">
							<span class="int"><input type="text" mthod="smt" name="${field.fieldName }" value="${field.defaultValue }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='1' && field.displayByAddMode==true }">
							<span class="int"><input type="text" mthod="smt" name="${field.fieldName }" value="${field.defaultValue }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='2' && field.displayByAddMode==true }">
							<span class="int"><textarea mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>${field.defaultValue }</textarea></span>
							</c:if>
							<c:if test="${field.tagType=='3' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==field.defaultValue }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='4' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==field.defaultValue }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='5' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==field.defaultValue }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							</span>
							<!--辅助控件部分-->
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByAddMode==true  }"><span class="assist"></c:if>
							<c:if test="${field.assistTags=='formatter' }">
								<input type="button" smt="formatter" name="${field.fieldName }_formatter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='fileSelecter' }">
								<input type="button" smt="fileSelecter" name="${field.fieldName }_fileSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='userSelecter' }">
								<input type="button" smt="userSelecter" name="${field.fieldName }_userSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='templateSelecter' }">
								<input type="button" smt="templateSelecter" name="${field.fieldName }_templateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateSelecter' }">
								<input type="button" smt="dateSelecter" name="${field.fieldName }_dateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='timeSelecter' }">
								<input type="button" smt="timeSelecter" name="${field.fieldName }_timeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateTimeSelecter' }">
								<input type="button" smt="dateTimeSelecter" name="${field.fieldName }_dateTimeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='colorSelecter' }">
								<input type="button" smt="colorSelecter" name="${field.fieldName }_colorSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByAddMode==true  }"></span></c:if>
						</td>
						
					</tr>
					</c:if>
				</c:forEach>
		</table>
    	
    </div>
    <div title="展现设置">
 	  	<table cellpadding="0" cellspacing="0" border="0">
				<c:forEach items="${view }" var="field">
				<c:if test="${field.displayByAddMode==true }">
					<tr <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
						<td class="tit" <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>><div class="tittext">${field.displayName }</div></td>
						<td class="val">
							
							<c:if test="${field.tagType=='0' && field.displayByAddMode==true }">
							<span class="int"><input type="text" mthod="smt" name="${field.fieldName }" value="${field.defaultValue }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='1' && field.displayByAddMode==true }">
							<span class="int"><input type="text" mthod="smt" name="${field.fieldName }" value="${field.defaultValue }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='2' && field.displayByAddMode==true }">
							<span class="int"><textarea mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>${field.defaultValue }</textarea></span>
							</c:if>
							<c:if test="${field.tagType=='3' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==field.defaultValue }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='4' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==field.defaultValue }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='5' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==field.defaultValue }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							</span>
							<!--辅助控件部分-->
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByAddMode==true  }"><span class="assist"></c:if>
							<c:if test="${field.assistTags=='formatter' }">
								<input type="button" smt="formatter" name="${field.fieldName }_formatter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='fileSelecter' }">
								<input type="button" smt="fileSelecter" name="${field.fieldName }_fileSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='userSelecter' }">
								<input type="button" smt="userSelecter" name="${field.fieldName }_userSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='templateSelecter' }">
								<input type="button" smt="templateSelecter" name="${field.fieldName }_templateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateSelecter' }">
								<input type="button" smt="dateSelecter" name="${field.fieldName }_dateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='timeSelecter' }">
								<input type="button" smt="timeSelecter" name="${field.fieldName }_timeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateTimeSelecter' }">
								<input type="button" smt="dateTimeSelecter" name="${field.fieldName }_dateTimeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='colorSelecter' }">
								<input type="button" smt="colorSelecter" name="${field.fieldName }_colorSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByAddMode==true  }"></span></c:if>
						</td>
						
					</tr>
					</c:if>
				</c:forEach>
 	  	</table>
    </div>
    <div title="模板信息">
 	  	<table cellpadding="0" cellspacing="0" border="0">
				<c:forEach items="${template }" var="field">
				<c:if test="${field.displayByAddMode==true }">
					<tr <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
						<td class="tit" <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>><div class="tittext">${field.displayName }</div></td>
						<td class="val">
							
							<c:if test="${field.tagType=='0' && field.displayByAddMode==true }">
							<span class="int"><input type="text" mthod="smt" name="${field.fieldName }" value="${field.defaultValue }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='1' && field.displayByAddMode==true }">
							<span class="int"><input type="text" mthod="smt" name="${field.fieldName }" value="${field.defaultValue }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>/></span>
							</c:if>
							<c:if test="${field.tagType=='2' && field.displayByAddMode==true }">
							<span class="int"><textarea mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>${field.defaultValue }</textarea></span>
							</c:if>
							<c:if test="${field.tagType=='3' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==field.defaultValue }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='4' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==field.defaultValue }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							<c:if test="${field.tagType=='5' && field.displayByAddMode==true }">
							<span class="int">
							<select mthod="smt" name="${field.fieldName }" <c:if test="${field.writeByEditMode==false }">readonly disabled</c:if> <c:if test="${field.displayByAddMode==false }">style="display:none;"</c:if>>
							<c:set value="${ fn:split(field.optionalValue, ';') }" var="values" />
								<c:forEach items="${values }" var="value">
									<c:set value="${ fn:split(value, ':') }" var="v" />
										<option value="${v[0] }" <c:if test="${v[0]==field.defaultValue }">selected</c:if>>${v[1] }</option>
								</c:forEach>
							</select>
							</span>
							</c:if>
							</span>
							<!--辅助控件部分-->
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByAddMode==true  }"><span class="assist"></c:if>
							<c:if test="${field.assistTags=='formatter' }">
								<input type="button" smt="formatter" name="${field.fieldName }_formatter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='fileSelecter' }">
								<input type="button" smt="fileSelecter" name="${field.fieldName }_fileSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='userSelecter' }">
								<input type="button" smt="userSelecter" name="${field.fieldName }_userSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='templateSelecter' }">
								<input type="button" smt="templateSelecter" name="${field.fieldName }_templateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateSelecter' }">
								<input type="button" smt="dateSelecter" name="${field.fieldName }_dateSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='timeSelecter' }">
								<input type="button" smt="timeSelecter" name="${field.fieldName }_timeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='dateTimeSelecter' }">
								<input type="button" smt="dateTimeSelecter" name="${field.fieldName }_dateTimeSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags=='colorSelecter' }">
								<input type="button" smt="colorSelecter" name="${field.fieldName }_colorSelecter" value="..."/>
							</c:if>
							<c:if test="${field.assistTags!='none' && field.assistTags!='' && field.displayByAddMode==true  }"></span></c:if>
						</td>
						
					</tr>
					</c:if>
				</c:forEach>
		</table>
    </div>
    </div>
    </div>
    </div>
    </body>
    <script type="text/javascript">
    var path='<%=path%>';
    $(document).ready(function(){
		bindEvents();
	});
	function bindEvents(){
		$("input[smt='formatter']").click(function(e){parent.showFormatterDialog(e);});
		$("input[smt='fileSelecter']").click(function(e){parent.showFileSelecterDialog(e);});
		$("input[smt='userSelecter']").click(function(e){parent.showUserSelecterDialog(e);});
		$("input[smt='templateSelecter']").click(function(e){parent.showTemplateSelecter(e);});
		$("input[smt='dateSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='timeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='dateTimeSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
		$("input[smt='colorSelecter']").click(function(e){});//不需要上级页面帮忙完成显示
	};	
	//由于本页面是嵌套在IFRAME中的，所以在展现一些对话框时，无法显示到本页（被挡住了，放不下），所以需要 调用上级页面的对话框显示函数，然后函数再调用下面的这个方法设置值
	/**
	*
	**/
	function setValue(name,val){
		$("[name='"+name+"']").val(val);
		$("[name='"+name+"']").blur();
	};
	
	String.prototype.replaceAll = function(s1,s2) { 
    	return this.replace(new RegExp(s1,"gm"),s2); 
	};
	function submit(){
		if($("#parentId").val()==""){
			alert("参数错误");
			return false;
		}
		
		
		var _s_tmp=$("[mthod='smt']");
		var params="";
		for(i=0;i<_s_tmp.size();i++){
			if(_s_tmp[i].tagName=="SELECT"){
				params+=""+(_s_tmp[i].name)+":'"+$(_s_tmp[i]).val()+"',";//.selectedOptions[0].value+"',";
			}else if(_s_tmp[i].tagName=="INPUT"){
				params+=""+(_s_tmp[i].name)+":'"+$(_s_tmp[i]).val()+"',";
			}else{
			}
		} 
		params="[{"+params+"parentId:"+$("#parentId").val()+"}]";
		
		$.post(
				path+"/Manage/Node/doAdd.do",
				eval(params)[0],
				function(result){
					var o=eval("("+result+")");
					if(o.status=="99"){
						return true;
					}else{
						alert(o.msg);
						return false;
					}
				}
			);
		return true;
	}
</script>