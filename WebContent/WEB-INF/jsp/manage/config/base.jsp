<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
			<table>
				<tr>
					<td class="tit">
						<span>${cNameMap.siteName}</span>
					</td>
					<td class="val">
						<span><input type="text" conf="true" name="siteName" value="${cValMap.siteName}"/></span>
					</td>
					<td class="rem">
						<span>${cRemMap.siteName}</span>
					</td>
				</tr>
				<tr>
					<td class="tit">
						<span>${cNameMap.siteKeys}</span>
					</td>
					<td class="val">
						<span><input type="text" conf="true" name="siteKeys" value="${cValMap.siteKeys}"/></span>
					</td>
					<td class="rem">
						<span>${cRemMap.siteKeys}</span>
					</td>
				</tr>
			
				<tr>
					<td class="tit">
						<span>${cNameMap.siteIntro}</span>
					</td>
					<td class="val">
						<span><input type="text" conf="true" name="siteIntro" value="${cValMap.siteIntro}"/></span>
					</td>
					<td class="rem">
						<span>${cRemMap.siteIntro}</span>
					</td>
				</tr>
			
        		<tr>
					<td class="tit">
						<span>${cNameMap.pageTemplateId}</span>
					</td>
					<td class="val">
						<span>
							<select conf="true" name="pageTemplateId">
								<option id="-1" <c:if test="cValMap.pageTemplateId==-1">selected</c:if>>默认模板</option>
								<c:forEach items="${pageTemplateList }" var="pt">
									<option id="${pt.id }" >${pt.title }</option>
								</c:forEach>
							</select>
							
						</span>
					</td>
					<td class="rem">
						<span>${cRemMap.pageTemplateId}</span>
					</td>
				</tr>
				<tr>
					<td class="tit">
						<span>${cNameMap.siteGray}</span>
					</td>
					<td class="val">
						<span>
							<select conf="true" name="siteGray">
								<option id="-1" <c:if test="cValMap.siteGray=='-1'">selected</c:if>>维持现状（不变灰）</option>
								<option id="0" <c:if test="cValMap.siteGray=='0'">selected</c:if>>仅首页变灰</option>
								<option id="1" <c:if test="cValMap.siteGray=='1'">selected</c:if>>首页+栏目页变灰</option>
								<option id="2" <c:if test="cValMap.siteGray=='2'">selected</c:if>>全站变灰</option>
							</select>
						
						</span>
					</td>
					<td class="rem">
						<span>${cRemMap.siteGray}</span>
					</td>
				</tr>
			
			
				<tr>
					<td class="tit">
						<span>${cNameMap.siteStatus}</span>
					</td>
					<td class="val">
						<span>
							<select conf="true" name="siteStatus">
								<option id="1" <c:if test="cValMap.siteStatus=='1'">selected</c:if>>开通访问</option>
								<option id="2" <c:if test="cValMap.siteStatus=='2'">selected</c:if>>禁止访问</option>
							</select>
						</span>
					</td>
					<td class="rem">
						<span>${cRemMap.siteStatus}</span>
					</td>
				</tr>
			</table>