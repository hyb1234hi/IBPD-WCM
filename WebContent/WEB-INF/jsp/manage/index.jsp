<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>${system_fullname }</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>css/easyui.css" />
		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>js/themes/icon.css" />
		<link type="text/css" href="<%=basePath %>css/forms.css" />
		<link href="<%=basePath %>css/default.css" rel="stylesheet" type="text/css" />
			
    <script type="text/javascript" src="<%=basePath %>js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="<%=basePath %>js/Validform_v5.3.2_ncr_min.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/custom.js"></script>
<script type="text/javascript">
	var basePath='<%=basePath%>';
//初始化菜单
	var _menus = [];

				<c:if test="${permissionEnable_userDisp==true}">
				_menus.push({"menuid":"12","menuname":"用户管理","icon":"icon-users","url":"<%=basePath %>Manage/User/index.do"});
				</c:if>
				<c:if test="${permissionEnable_roleDisp==true}">
				_menus.push({"menuid":"13","menuname":"角色管理","icon":"icon-users","url":"<%=basePath %>Manage/Role/index.do"});
				</c:if>
				<c:if test="${permissionEnable_paramDisp==true}">
				_menus.push({"menuid":"14","menuname":"参数设置","icon":"icon-role","url":"<%=basePath %>/Manage/SysProper/index.do"});
				</c:if>
			
    </script>
	<script type="text/javascript" src='<%=basePath %>js/mgrIndex.js'> </script>
			

	</head>

	<body>
	<noscript>
<div style=" position:absolute; z-index:100000; height:2046px;top:0px;left:0px; width:100%; background:white; text-align:center;">
    <img src="images/noscript.gif" alt='抱歉，请开启脚本支持！' />
</div></noscript>
	<div class="easyui-layout" fit="true">
	    <div region="north" split="true" border="false" style="overflow: hidden; height: 30px;
        background: url(images/layout-browser-hd-bg.gif) #7f99be repeat-x center 50%;
        line-height: 20px;color: #fff; font-family: Verdana, 微软雅黑,黑体">
        <span style="float:right; padding-right:20px;" class="head">${current_calender} &nbsp;&nbsp;欢迎 管理员 ${loginedUserName } <a href="javascript:void()" id="editpass">修改密码</a> <a href="javascript:void()" id="loginOut">安全退出</a></span>
        <span style="padding-left:10px; font-size: 16px; ">
        <img src="images/blocks.gif" width="20" height="20" align="absmiddle" /> ${header_text }</span>
    </div>
    <div region="south" split="true" style="height: 30px; background: #D2E0F2; ">
        <div class="footer">${footer_text }</div>
    </div>
	
   <div region="west" hide="true" split="true" title="站点导航" style="width:200px;" id="west">
		<div id="lefttree" class="easyui-accordion" fit="true" border="false" >
			<div id="nodetree" title="站点栏目" data-options="tools:[{iconCls:'icon-reload',handler:function(){$('#nodetree').tree('reload');}}]"></div>
			<div id="shoptree" title="商城管理" data-options="tools:[{iconCls:'icon-reload',handler:function(){$('#shoptree').tree('reload');}}]"></div>
			<div id="syscfg" title="系统设置"></div>
		</div>
		
    </div>
    <div id="nodeview" region="center" style="background: #eee; overflow-y:hidden">
        <div id="tabs" tabPosition="top" class="easyui-tabs"  fit="true" border="false" >
		</div>
    </div>
	</div>
	<div id="mm" class="easyui-menu" style="width:120px;display:none">
	    <div onClick="ShowEditOrViewDialog()" data-options="iconCls:'icon-edit'">编辑</div>
	    <div onClick="del()" data-options="iconCls:'icon-remove'">删除</div>
	    <div onClick="reload()" data-options="iconCls:'icon-reload'">刷新</div>
	</div>
	<div id="nodeCtxMenu" class="easyui-menu" style="width:150px;display:none">
	    <div onClick="ShowAddDialog()" data-options="iconCls:'icon-add'">添加子栏目</div>
	    <div onClick="ShowEditDialog()" data-options="iconCls:'icon-edit'">编辑该栏目</div>
	    <div onClick="PublishNodePage(false)" data-options="iconCls:'icon-publish'">发布该栏目页面</div>
	    <div onClick="PublishNodePage(true)" data-options="iconCls:'icon-publish'">发布该栏目页面和内容</div>
	    <div onClick="ShowEditOrViewDialog()" data-options="iconCls:'icon-addcontent'">添加栏目内容 </div>
	    <div onClick="ShowEditOrViewDialog()" data-options="iconCls:'icon-state'">开启/关闭该栏目 </div>
	    <div onClick="ShowEditOrViewDialog()" data-options="iconCls:'icon-nonavi'">不在导航中显示 </div>
	    <div onClick="del()" data-options="iconCls:'icon-remove'">删除该栏目</div>
	    <div onClick="reload()" data-options="iconCls:'icon-reload'">刷新栏目列表</div>
	</div>
		<div id="tabMenu" class="easyui-menu" style="width:150px;display:none">
		<div id="mm-tabupdate">刷新</div>
		<div class="menu-sep"></div>
		<div id="mm-tabclose">关闭</div>
		<div id="mm-tabcloseall">全部关闭</div>
		<div id="mm-tabcloseother">除此之外全部关闭</div>
		<div class="menu-sep"></div>
		<div id="mm-tabcloseright">当前页右侧全部关闭</div>
		<div id="mm-tabcloseleft">当前页左侧全部关闭</div>
		<div class="menu-sep"></div>
		<div id="mm-exit">退出</div>
	</div>
	
	<script type="text/javascript">
	$(document).ready(function(){
		initNodeTree();
		initShopTree();
		<c:if test="${loginedUserType=='tenant'}">
		$("#lefttree").accordion("remove",0);
		$("#lefttree").accordion("remove",1);
		</c:if>
		$("#loginOut").click(function(){logout();});
		$("#editpass").click(function(){editPassword();});
	});
	function logout(){
		$.messager.confirm("提示","确认退出吗?",function(r){
			if(r){
				$.post(
					basePath+"Manage/User/doLogout.do",
					{},
					function(result){
						location.href=basePath+"/Manage/index.do?t="+new Date();
					}
				);
			}
		});
	};
	function editPassword(){
		var username='${loginedUserName }';
		if(username=='superadmin'){
			msgShow("提示","请求不被允许.","info");
			return;
		}
        var editDialog = $('<div id="editNodeDiv"/>').appendTo('body');
        $(editDialog).dialog({
        	modal:true,
        	title:'修改密码',
        	shadow:true,
        	iconCls:'icon-edit',
        	width:550,
        	height:400,
        	resizable:true,
        	toolbar:[{
                    text:'修改',
                    iconCls:'icon-save',
                    handler:function(){
                        if($("#editiframe")[0].contentWindow.submit()){
	                        $(editDialog).dialog("close");
	                        $(editDialog).remove();
							reload();
                        }else{
							msgShow("错误","操作失败","error");
							
                        }
                    }
                },'-',{
                    text:'取消',
                    iconCls:'icon-cancel',
                    handler:function(){
                         $(editDialog).dialog("close");
	                     $(editDialog).remove();
                    }
                }],
        	content:'<iframe id="editiframe" width="500px" height="300px" scrolling="no" frameborder="no" style="overflow:hidden;" src="'+basePath+'/Manage/User/toEditCurrentUserPassword.do"></iframe>'
        });
        $(editDialog).dialog("open");
    };

	function PublishNodePage(s){
		var treeNode=$('#nodetree').tree('getSelected');
		if(treeNode.id.indexOf('site_')!=-1){
			$.post(
				basePath+"Manage/Node/publishSite.do",
				{id:treeNode.id.split("_")[1]},
				function(result){
					msgShow("提示","发布请求已经提交到后台.","info");
				}
			);
		}else{
			$.post(
				basePath+"Manage/Node/publishTree.do",
				{id:treeNode.id,publishContent:s},
				function(result){
					msgShow("提示","发布请求已经提交到后台.","info");
				}
			);
		}

		
	};
	function reloadNodeTree(){
		$('#nodetree').tree("reload");
	};
	function ShowAddDialog(){
		var tb=$('#tabs').tabs("getTab","综合管理");
		var treeNode=$('#nodetree').tree('getSelected');
		if(tb==null){
			addTab('综合管理',basePath+'Manage/Node/index.do?id='+treeNode.id+'&t='+new Date(),'');
		}
		setTimeout(function(){
			var tb=$('#tabs').tabs("getTab","综合管理");
			tb.find("iframe")[0].contentWindow.ShowAddDialog(treeNode.id);
		},2000);
	};
	function ShowEditDialog(){
		var tb=$('#tabs').tabs("getTab","综合管理");
		var treeNode=$('#nodetree').tree('getSelected');
		if(tb==null){
			addTab('综合管理',basePath+'Manage/Node/index.do?id='+treeNode.id+'&t='+new Date(),'');
		}
		setTimeout(function(){
			var tb=$('#tabs').tabs("getTab","综合管理");
			tb.find("iframe")[0].contentWindow.ShowEditDialog(treeNode.id);
		},2000);
	};
	function initNodeTree(){
		$('#nodetree').tree({
          url:basePath+'Manage/Node/tree.do?t='+new Date(),
          //checkbox:true,
          
          onClick:function(node){
            //alert(node.id);
            var tabTitle = '综合管理';//node.text;
			$('#tabs').tabs('close',tabTitle);

		var url = basePath+"Manage/Node/index.do?id="+node.id+"&t="+new Date();
		var icon = '';//getIcon(menuid,icon);
		if(node.id=='site_all'){
			url=basePath+"Manage/SubSite/index.do?id="+node.id+"&t="+new Date();
			icon='';
		}
		addTab(tabTitle,url,icon);
		$('.easyui-accordion li div').removeClass("selected");
		$(this).parent().addClass("selected");
			},
          onContextMenu: function(e, node){
          	e.preventDefault();
            if(node.id=="-1"){
          	return;
          	}
          	$('#nodetree').tree('select', node.target);
          	$('#nodeCtxMenu').menu('show', {
				left:e.pageX,
				top:e.pageY
			});  
          }  
       });
   };
	function initShopTree(){
		$('#shoptree').tree({
          url:basePath+'Manage/Catalog/tree.do?t='+new Date(),
          //checkbox:true,
          
          onClick:function(node){
            //alert(node.text);
            var tabTitle = '';//node.text;
			var url = "";
			if(node.id>=0){
				tabTitle = '商城管理';
				url = basePath+"Manage/Catalog/index.do?id="+node.id+"&t="+new Date();
			}else if(node.id==-2){
				tabTitle = '订单管理';
				url = basePath+"Manage/Order/index.do";
			}else if(node.id==-21){
			//已下单未审核订单
				tabTitle = '订单管理';
				url = basePath+"Manage/Order/index.do?status=init&paystatus=n";
			}else if(node.id==-22){
			//已审核未付款订单
				tabTitle = '订单管理';
				url = basePath+"Manage/Order/index.do?status=pass&paystatus=n";
			}else if(node.id==-23){
			//已付款未发货订单
				tabTitle = '订单管理';
				url = basePath+"Manage/Order/index.do?status=pass&paystatus=y";
			}else if(node.id==-24){
			//已发货未签收订单
				tabTitle = '订单管理';
				url = basePath+"Manage/Order/index.do?status=send&paystatus=y";
			}else if(node.id==-25){
			//已签收未归档订单
				tabTitle = '订单管理';
				url = basePath+"Manage/Order/index.do?status=sign&paystatus=y";
			}else if(node.id==-26){
			//已归档订单
				tabTitle = '订单管理';
				url = basePath+"Manage/Order/index.do?status=file&paystatus=y";
			}else if(node.id==-27){
			//已取消订单
				tabTitle = '订单管理';
				url = basePath+"Manage/Order/index.do?status=cancel&paystatus=n";
			}else if(node.id==-28){
			//退款退货订单
				tabTitle = '退款退货';
				url = basePath+"Manage/Order/refund.do";
			}else if(node.id==-3){
				tabTitle = '会员管理';
				url = basePath+"Manage/Account/index.do";
			}else if(node.id==-4){
				tabTitle = '企业商户';
				url = basePath+"Manage/Tenant/index.do?type=ent";
			}else if(node.id==-41){
				tabTitle = '企业商户管理';
				url = basePath+"Manage/Tenant/index.do?type=ent";
			}else if(node.id==-42){
				tabTitle = '代理商户';
				url = basePath+"Manage/Tenant/index.do?type=sing";
			}else if(node.id==-5){
				tabTitle = '销售分析';
				url = basePath+"Manage/Sell/index.do";
			}else if(node.id==-6){
				tabTitle = '快递方式';
				url = basePath+"Manage/Exp/index.do";
			}else if(node.id==-7){
				tabTitle = '幻灯设置';
				url = basePath+"Manage/MallHomePage/index.do?type=pop";
			}else if(node.id==-71){
				tabTitle = '幻灯设置';
				url = basePath+"Manage/MallHomePage/index.do?type=pop";
			}else if(node.id==-72){
				tabTitle = '今日推荐';
				url = basePath+"Manage/MallHomePage/index.do?type=jrtj";
			}else if(node.id==-73){
				tabTitle = '热门市场';
				url = basePath+"Manage/MallHomePage/index.do?type=rmsc";
			}else if(node.id==-74){
				tabTitle = '会员专卖';
				url = basePath+"Manage/MallHomePage/index.do?type=hyzm";
			}
				$('#tabs').tabs('close',tabTitle);

				var icon = '';//getIcon(menuid,icon);

				addTab(tabTitle,url,icon);
				$('.easyui-accordion li div').removeClass("selected");
				$(this).parent().addClass("selected");
			},
          onContextMenu: function(e, node){
          
          }  
       });
   };
     </script>
	</body>
</html>
