$.fn.customGrid = function(options) {
	var defaults = {
                title: '',
                title: '',
                iconCls: 'icon-grid',
                singleSelect:false,
                fit:true,
                width: function () { return document.body.clientWidth * 0.98 },
                nowrap: true,
                striptd:true,
                loadMsg:'数据加载中,请稍后……',
                autoRowHeight: false,
                striped: true,
                collapsible: false,
                pagination: true,
                pageSize: 10,
                pageList: [10,20,30,50,100],
                rownumbers: true,
                //sortName: 'ID',    //根据某个字段给easyUI排序
                sortOrder: 'asc',
                remoteSort: true,
                fitColumns:true,
                idField: 'id'
 	};
	var id = $(this).attr('id');
	options = $.extend(defaults, options);
	var self = this;
	var jid="#"+id;
	$(self).datagrid({
                iconCls: 'icon-grid',
                singleSelect:options.singleSelect,
                fit:options.fit,
                width: function () { return document.body.clientWidth * 0.98 },
                nowrap: true,
                striptd:true,
                loadMsg:'数据加载中,请稍后……',
                autoRowHeight: false,
                striped: true,
                collapsible: false,
                pagination: true,
                pageSize: options.pageSize,
                pageList: options.pageList,
                rownumbers: options.rownumbers,
                sortName: options.sortName,   
                sortOrder: options.sortOrder,
                remoteSort: true,
                fitColumns:true,
                idField: options.idField,
                url: options.url, 
                title: options.title,
                queryParams: options.queryData,  //异步查询的参数 
                onRowContextMenu:function(e, rowIndex, rowData){
        			e.preventDefault();
        			//$('#grid').datagrid('uncheckAll');
                    $('#'+id).datagrid('checkRow', rowIndex);
				    $('#'+options.rowCtxMenuId).menu('show', {
				        left:e.pageX,
				        top:e.pageY
				    });    
   				},
                columns: options.columns, 
                toolbar: options.toolbar,
               onDblClickRow: function (rowIndex, rowData) {
                    $('#'+id).datagrid('uncheckAll');
                    $('#'+id).datagrid('checkRow', rowIndex);
                },
                onClickRow: function (rowIndex, rowData) {
                	loadProps(options.propPanelId,options.propUrl.replace("{id}",rowData.id));
                    $('#'+id).datagrid('uncheckAll');
                    $('#'+id).datagrid('checkRow', rowIndex);
                },  
                onHeaderContextMenu: function(e, field){
                    e.preventDefault();
                    if (!cmenu){
                        createColumnMenu(id);
                    }
                    cmenu.menu('show', {
                        left:e.pageX,
                        top:e.pageY
                    });
                }
 	});
	$(window).resize(function(){
		$(self).datagrid('resize', {
			width:function(){return document.body.clientWidth * 0.98;},
			height:function(){return $(self).parent.height;},
		});
	});
}